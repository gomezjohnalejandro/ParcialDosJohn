﻿using ParcialDosJohn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParcialDosJohn
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListPersonas : ContentPage
	{
		public ListPersonas ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {

                List<Personas> listaPersonas;
                listaPersonas = connection.Table<Personas>().ToList();

                listViewPersonas.ItemsSource = listaPersonas;

            }
        }

    }
}