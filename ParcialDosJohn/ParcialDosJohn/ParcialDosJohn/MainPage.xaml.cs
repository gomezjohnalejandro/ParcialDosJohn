﻿using ParcialDosJohn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ParcialDosJohn
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        public void ButtonClick(object sender, EventArgs e)
        {

            // crear objeto del modelo tarea
            Personas personas= new Personas()
            {
                Name = name.Text,
                DateFinish = dateFinish.Date,
                HourFinish = timeFinish.Time,
                Completed = false
            };

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Personas>();

                // crear registro en la tabla
                var result = connection.Insert(personas);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "La Persona se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "La Persona no fue creada", "OK");
                }
            }
        }


        async public void ListWindow(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListPersonas());
        }

    }
}
